module Main (main) where

import Debug.Trace

type Game = ([Int],[Int])

main :: IO ()
main = do
        file <- readFile "input.txt"
        let cards = fmap ((both (fmap (read :: String -> Int) . words)) . (\(a,b) -> (a,drop 1 b)) . (splitAt 30) . (drop 10)) $ lines file
        putStrLn . ("Result (part 1): " ++) $ show $ sum $ fmap getScore cards
        putStrLn . ("Result (part 2): " ++) $ show $ sum $ fmap (getScoreRec cards) (toIndeces cards)

both :: (a -> b) -> (a,a) -> (b,b)
both f (a,b) = (f a,f b)

getScore :: Game -> Int
getScore g = if x >= 0 then 2 ^ x else 0
                where x = getNumSame g - 1


getNumSame :: Game -> Int
getNumSame (a,b) = length $ filter (\s -> elem s a) b

-- Part 2

getScoreRec :: [Game] -> Int -> Int
getScoreRec gs i = 1 + (sum $ fmap (getScoreRec gs) (take s $ drop (i + 1) $ toIndeces gs))
                where s = getNumSame $ gs !! i

toIndeces :: [a] -> [Int]
toIndeces [] = []
toIndeces a = [0..pred $ length a]