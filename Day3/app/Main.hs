module Main (main) where

import Data.Char
import Data.Maybe
import qualified Data.List as L
import Control.Applicative ()
import Control.Monad

type SENumber = (Int,(Int,Int,Int))

main :: IO ()
main = do
        file <- readFile "input.txt"
        let games = lines file
        putStrLn . ("Result (part 1): " ++) $ show $ sum $ fmap fst $ L.nub $ concat $ getNumbers isMySymbol games
        putStrLn . ("Result (part 2): " ++) $ show $ sum $ fmap (\[a,b] -> a * b) $ filter (\s -> length s == 2) $ (fmap . fmap) fst $ getNumbers isGear games

getPoint :: Int -> Int -> [[a]] -> a
getPoint x y = (!! x) . (!! y)

insideBounds :: Int -> Int -> [[a]] -> Bool
insideBounds x y sa | x >= 0 && y >= 0 && x < (length $ head sa) && y < (length sa) = True
insideBounds _ _ _ = False

isGear :: Char -> Bool
isGear = (== '*')

isMySymbol :: Char -> Bool
isMySymbol '.' = False
isMySymbol c = not $ isDigit c

getNumbers :: (Char -> Bool) -> [String] -> [[SENumber]]
getNumbers f s = [getNeighbourNumbers x y s | y <- [0..(pred . length)s], x <- [0..(pred . length)(s !! y)], f (getPoint x y s)]

getNeighbourNumbers :: Int -> Int -> [String] -> [SENumber]
getNeighbourNumbers x y = concat . fmap L.nub . getNeighbourNumbersSE x y

getNeighbourNumbersSE :: Int -> Int -> [String] -> [[SENumber]]
getNeighbourNumbersSE x y sa = do
                        offsetY <- [-1..1]
                        let ny = (y + offsetY)
                        return $ do
                                offsetX <- [-1..1]
                                let nx = (x + offsetX)
                                guard $ insideBounds nx ny sa
                                catMaybes [tryParseNumberBidirectional nx ny sa]

tryParseNumberBidirectional :: Int -> Int -> [String] -> Maybe SENumber
tryParseNumberBidirectional x y s | isDigit $ getPoint x y s = Just $ (parseNumberBidirectional x (s !! y), findStartEnd x y s)
tryParseNumberBidirectional _ _ _ = Nothing

parseNumberBidirectional :: Int -> String -> Int
parseNumberBidirectional i s = read $ takeDigits $ drop (findStart i s) s

findStartEnd :: Int -> Int -> [String] -> (Int,Int,Int)
findStartEnd x y sa = (findStart x s, findEnd x s, y)
                        where s = sa !! y

findStart :: Int -> String -> Int
findStart 0 (c:_) = if isDigit c then 0 else 1
findStart i s | isDigit $ s !! i = findStart (i - 1) s
findStart i _ = i + 1

findEnd :: Int -> String -> Int
findEnd i s = i + (length $ takeDigits (drop i s)) - 1

takeDigits :: String -> String
takeDigits = takeWhile isDigit