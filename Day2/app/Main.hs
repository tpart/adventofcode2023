module Main (main) where

import GHC.Utils.Misc
import Data.List

type Cubes = [(Integer, String)]
type Game = (Integer, [Cubes])

main :: IO ()
main = do
        file <- readFile "input.txt"
        let games = fmap parseGame $ lines file
        putStrLn . ("Input: " ++) $ show games
        putStrLn . ("Result (part 1): " ++) . show . sumOfTrueIds . fmap (onSnd (and . fmap (verifyCubeCount 12 13 14 . countCubes))) $ games
        putStrLn . ("Result (part 2): " ++) . show . powerSets . fmap (fewestCubeCount . fmap countCubes) $ fmap snd $ games


parseGame :: String -> Game
parseGame = onBoth (read . drop 5) parseReveal . makeTuple . trimmedSplit ':'

parseReveal :: String -> [Cubes]
parseReveal = fmap parseCubes . trimmedSplit ';'

parseCubes :: String -> Cubes
parseCubes = fmap (onFst read . makeTuple . trimmedSplit ' ') . trimmedSplit ','

trimmedSplit :: Char -> String -> [String]
trimmedSplit c = fmap trimSpaces . split c . trimSpaces

countCubes :: Cubes -> [Integer]
countCubes ((n,"red"):as) = [n,0,0] `addArrays` countCubes as
countCubes ((n,"green"):as) = [0,n,0] `addArrays` countCubes as
countCubes ((n,"blue"):as) = [0,0,n] `addArrays` countCubes as
countCubes [] = [0,0,0]

verifyCubeCount :: Integer -> Integer -> Integer -> [Integer] -> Bool
verifyCubeCount rm gm bm [r,g,b] | r > rm || g > gm || b > bm = False
verifyCubeCount _ _ _ _ = True

fewestCubeCount :: [[Integer]] -> [Integer]
fewestCubeCount = fmap maximum . transpose

addArrays :: Num a => [a] -> [a] -> [a]
addArrays [a] [b] = [a+b]
addArrays (a:as) (b:bs) = (a+b) : addArrays as bs

sumOfTrueIds :: [(Integer, Bool)] -> Integer
sumOfTrueIds [] = 0
sumOfTrueIds ((a, True):as) = a + sumOfTrueIds as
sumOfTrueIds ((a, False):as) = sumOfTrueIds as

trimSpaces :: String -> String
trimSpaces (' ':as) = trimSpaces as
trimSpaces a = a

onBoth :: (a -> b) -> (c -> d) -> (a,c) -> (b,d)
onBoth f g (a,b) = (f a,g b)

onFst :: (a -> b) -> (a,c) -> (b,c)
onFst f (a,b) = (f a,b)

onSnd :: (a -> b) -> (c,a) -> (c,b)
onSnd f (a,b) = (a,f b)

powerSets :: [[Integer]] -> Integer
powerSets = sum . fmap product

makeTuple :: [a] -> (a, a)
makeTuple [a, b] = (a, b)
makeTuple _ = error "invalid input"
