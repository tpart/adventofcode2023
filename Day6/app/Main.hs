module Main (main) where

main :: IO ()
main = do
        fd <- (fmap . fmap) (tail . words) $ fmap lines $ readFile "input.txt"
        let inputs = makeTuples $ (fmap . fmap) (read :: String -> Integer) fd
        putStrLn $ "Inputs (part 1): " ++ show inputs
        let outputs = fmap (uncurry calculateNum) inputs
        putStrLn $ "Outputs (part 1): " ++ show outputs
        putStrLn $ ("Solution (part 1): " ++) $ show $ product outputs
        let inputs2 = makeTuple $ fmap (read :: String -> Integer) $ fmap concat $ fd
        putStrLn $ ("Inputs (part 2): " ++) $ show inputs2
        let outputs2 = uncurry calculateNum inputs2
        putStrLn $ "Solution (part 2): " ++ show outputs2

makeTuple :: [a] -> (a, a)
makeTuple [a, b] = (a, b)
makeTuple _ = error "invalid input"

makeTuples :: [[a]] -> [(a, a)]
makeTuples [[a], [b]] = [(a, b)]
makeTuples [a : as, b : bs] = (a, b) : makeTuples [as, bs]
makeTuples _ = error "invalid input"

calculateNum :: (Ord a, Num a, Enum a) => a -> a -> Int
calculateNum m r = length $ filter (> r) $ fmap (\t -> pressButton t m) [0..m]

pressButton :: Num a => a -> a -> a
pressButton p t = p * (t - p)