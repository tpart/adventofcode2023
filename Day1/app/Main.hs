module Main (main) where

import Data.Char

main :: IO ()
main = do
        fd <- fmap lines $ readFile "input.txt"
        putStrLn $ ("Sum (part 1): " ++) $ show $ sum $ fmap getFirstAndLast $ (fmap . fmap) toDigit fd
        putStrLn $ ("Sum (part 2): " ++) $ show $ sum $ fmap getFirstAndLast $ fmap getNumbers2 fd


startsWith :: String -> String -> Bool
startsWith a b | a == b = True
startsWith "" _ = False
startsWith a b = startsWith (init a) b


getNumbers2 :: [Char] -> [Int]
getNumbers2 [c] = [toDigit c]
getNumbers2 s = (toDigit2 s :) $ getNumbers2 $ tail s


getFirstAndLast :: [Int] -> Int
getFirstAndLast s =
                let x = filter (/=(-1)) s
                in head x * 10 + last x


toDigit :: Char -> Int
toDigit c = if isDigit c then digitToInt c else (-1)


toDigit2 :: [Char] -> Int
toDigit2 s | s `startsWith` "one" = 1
toDigit2 s | s `startsWith` "two" = 2
toDigit2 s | s `startsWith` "three" = 3
toDigit2 s | s `startsWith` "four" = 4
toDigit2 s | s `startsWith` "five" = 5
toDigit2 s | s `startsWith` "six" = 6
toDigit2 s | s `startsWith` "seven" = 7
toDigit2 s | s `startsWith` "eight" = 8
toDigit2 s | s `startsWith` "nine" = 9
toDigit2 (s:_) = toDigit s